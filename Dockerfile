# BUILD STAGE
FROM alpine:latest AS builder
WORKDIR /app
COPY . /app
RUN apk add --no-cache make build-base sqlite-dev
RUN make

# BUILD STAGE
FROM alpine:latest
WORKDIR /app
COPY --from=builder /app .
RUN apk add --no-cache sqlite-dev
RUN ln -sf /proc/1/fd/1 psp.log
EXPOSE 27312
CMD ["sh", "-c","/app/AdhocServer > /app/psp.log"] <-------error in stdout

##Build
#docker build -t psp-adhocserver:alpine . --no-cache
##Run
#docker run -p 27312:27312 -dit psp-adhocserver:alpine
