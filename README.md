# AdhocServer Pro

A lightly modified version of the original server.

This version adds a FIFO pipe (linux only) to allow another application to hook into when players enter of leave rooms.

Version obtained from:
http://forums.ppsspp.org/showthread.php?tid=3595&pid=59241#pid59241

This version is licensed under GPLv3, as the original

#Deploy AdhocServer in Docker 

#Get the Image from Docker Hub

https://hub.docker.com/r/s2031215/adhocserverpro

`docker pull s2031215/adhocserverpro`


`docker run --name pspserver -p 27312:27312 -dit s2031215/adhocserverpro`

#Build the Image By yourself

Build Image

`docker build -t psp-adhocserver:alpine . --no-cache`

Run Image

`docker run -p 27312:27312 -dit psp-adhocserver:alpine`
